<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Asteroid - Neo Stats</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

</head>

<body>
    <h2 class="text-center ">Asteroid - Neo Stats </h2>
    <div class="container mt-8">

        <form action="{{ route('index') }}" class="row" id="todoForm">
            <input type="text" name="is_search" value='1' hidden>
            <div class="col-lg-4">
                <label for="fromdate" class="form-label">From Date</label>
                <input placeholder="From Date" type="date" id="fromdate" class="form-control"
                    name="fromdate" @if(isset($fromdate)) value="{{ $fromdate }}" @endif required>
            </div>
            <div class="col-lg-4">
                <label for="todate" class="form-label">To Date</label>
                <input placeholder="To Date" type="date" id="todate" class="form-control"
                    name="todate" @if(isset($todate)) value="{{ $todate }}" @endif required>
            </div>
            <div class="col-lg-4 mt-4">
                <button type="submit" class="btn btn-success" id="search">Search</button>
                <a href="{{ route('index') }}" class="btn btn-primary">Reset</a>
            </div>
        </form>

        @if($is_search)
        <div class="container mt-4">
            <div class="alert alert-primary" role="alert">
                Asteroid - Neo Stats From Date: {{ $fromdate }} To Date: {{ $todate }}
              </div>
        </div>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">Fastest Asteroid Id</th>
                <th scope="col">Speed(in KM/Hour)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{{ $fastestAseroidId }}</td>
                <td>{{ $fastestAseroid }}</td>
              </tr>
              
            </tbody>
          </table>
          <table class="table">
            <thead>
              <tr>
                <th scope="col">Closest Asteroid Id</th>
                <th scope="col">Distance(in KM)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{{ $closestAseroidId }}</td>
                <td>{{ $closestAseroid }}</td>
              </tr>
              
            </tbody>
          </table>
          <h6>Average Size of the Asteroids (in kilometers) Accordding to the diameter: {{ $average_of_all_asteroids }}</h6> 
        <h4 class="mt-4"> Graphical Representation of Number of Asteroids </h4>
        <div  class="container ml-5" >
            <canvas id="myChart" width="400" height="400" style="border: solid;color: red;"></canvas>
        </div>
        @endif
    </div>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
        integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"
        integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous">
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>

    <script>
        var noOfAstroids = <?php  if(isset($neo_count_by_date_arry_values)) { echo json_encode($neo_count_by_date_arry_values); } ?>;
        var astroidsAppeardate = <?php  if(isset($neo_count_by_date_arry_keys)) { echo json_encode($neo_count_by_date_arry_keys); } ?>;
       //alert(astroidsAppeardate[0]);
        
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels:astroidsAppeardate,
                //labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [
                    {
                        label: '# of Asteroids',
                        // lineTension:1,
                        data: noOfAstroids,
                        //data: [12, 19, 3, 5, 2, 3],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }
                ]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
   
</body>

</html>
